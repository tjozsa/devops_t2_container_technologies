## Deploying Apache2 web server

You can find the Apache2 official image here:
[Apache2 official image on dockerhub](https://hub.docker.com/_/httpd)

```bash
docker image pull httpd
```

You should see a similar output:
```
Using default tag: latest
latest: Pulling from library/httpd
bb79b6b2107f: Pull complete 
26694ef5449a: Pull complete 
7b85101950dd: Pull complete 
da919f2696f2: Pull complete 
3ae86ea9f1b9: Pull complete 
Digest: sha256:b82fb56847fcbcca9f8f162a3232acb4a302af96b1b2af1c4c3ac45ef0c9b968
Status: Downloaded newer image for httpd:latest
docker.io/library/httpd:latest
```

Now let's run the following command:

```bash
docker container run -d -p 80:80 httpd
```

You should see a contianer identifier as an output:
```bash
ebc61500cf5ac241c3410b71afb67e686b4e83481007f6bb17af4a9dbd1966df
```

You should see that a new port `80` link appeard.

![](img/open_port.png)

Click this link to open a new tab where the output from the container will be visible

![](img/it_workds.png)

Let's check the running containers

```bash
docker container ps
```

should output something similar
```bash
CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS                NAMES
ebc61500cf5a        httpd               "httpd-foreground"   9 seconds ago       Up 7 seconds        0.0.0.0:80->80/tcp   serene_agnesi
```

Let's check the logs of the container
```bash
docker container logs <container_id>
```

should output something similar
```bash
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
[Mon Oct 26 07:17:06.750206 2020] [mpm_event:notice] [pid 1:tid 140369727775872] AH00489: Apache/2.4.46 (Unix) configured -- resuming normal operations
[Mon Oct 26 07:17:06.750440 2020] [core:notice] [pid 1:tid 140369727775872] AH00094: Command line: 'httpd -D FOREGROUND'
172.18.0.1 - - [26/Oct/2020:07:18:32 +0000] "GET / HTTP/1.1" 200 45
172.18.0.1 - - [26/Oct/2020:07:18:34 +0000] "GET /favicon.ico HTTP/1.1" 404 196
```
