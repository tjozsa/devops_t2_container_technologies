## Your assignments if you accept them.

All of these assigments are online lab supported tutorials. They are excellent support materials for the workshops you participated in.

1. Docker for System Administrators tutorial series:
    * [First Alpine Linux Containers](https://training.play-with-docker.com/ops-s1-hello/)
    * [Doing More With Docker Images](https://training.play-with-docker.com/ops-s1-images/)
    * [Swarm Mode Introduction for IT Pros](https://training.play-with-docker.com/ops-s1-swarm-intro/)

1. Kubernetes: 
    * [Kubernetes basics tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

1. Extra labs can be found on katacoda.com