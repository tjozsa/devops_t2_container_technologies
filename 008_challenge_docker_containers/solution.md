## Create a Dockerfile for our own Nginx image 

```Dockerfile
FROM nginx
COPY index.html /usr/share/nginx/html
```

## Create a simple HTML file 

Place this file under the the same folder where you have your Dockerfile

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Hello NGinx!</h1>
</body>
</html>
```


## Build our version of the Nginx image with the new HTML content

```bash
docker build -t my-nginx .
```

You should see a similar output:
```
Sending build context to Docker daemon  8.192kB
Step 1/2 : FROM httpd:2.4
2.4: Pulling from library/httpd
bb79b6b2107f: Pull complete 
26694ef5449a: Pull complete 
7b85101950dd: Pull complete 
da919f2696f2: Pull complete 
3ae86ea9f1b9: Pull complete 
Digest: sha256:b82fb56847fcbcca9f8f162a3232acb4a302af96b1b2af1c4c3ac45ef0c9b968
Status: Downloaded newer image for httpd:2.4
 ---> 3dd970e6b110
Step 2/2 : COPY ./public-html/ /usr/local/apache2/htdocs/
 ---> c1b7c9678130
Successfully built c1b7c9678130
Successfully tagged my-apache2:latest
```

## Start our version of the Nginx image with the new HTML content

```bash
docker run -dit --name my-running-app -p 80:80 my-nginx
```

You should see a similar output:
```
8945d4733d3ad7ca1a7046709737474a863b37ffd8bd16845e0ed06eddf3e949
```



You should see that a new port `80` link appeard.

![](img/open_port.png)

Click this link to open a new tab where the output from the container will be visible

![](img/my_worked.png)