## Basic Terms: System Parts
* Kubernetes: The whole orchestration system
* K8s "k-eights" or Kube for short
* Kubectl: CLI to configure Kubernetes and manage apps
* Using "cube control" official pronunciation
* Node: Single server in the Kubernetes cluster
* Kubelet: Kubernetes agent running on nodes
* Control Plane: Set of containers that manage the cluster
* Includes API server, scheduler, controller manager, etcd, and more
* Sometimes called the "master"

## Install Kubernetes Locally
* Kubernetes is a series of containers, CLI's, and configurations
* Many ways to install, lets focus on easiest for learning
* Docker Desktop: Enable in settings
* Sets up everything inside Docker's existing Linux VM
* Docker Toolbox on Windows: MiniKube
* Uses VirtualBox to make Linux VM
* Your Own Linux Host or VM: MicroK8s
* Installs Kubernetes right on the OS

## Kubernetes In A Browser
* Try [k8s playground](http://play-with-k8s.com) or [katacoda](http://katacoda.com) in a browser
* Easy to get started
* Doesn't keep your environment

## Docker Desktop
* Runs/configures Kubernetes Master containers
* Manages kubectl install and certs
* Easily install, disable, and remove from Docker GUI

## MiniKube
* Download Windows Installer from GitHub
* minikube-installer.exe
* minikube start
* Much like the docker-machine experience
* Creates a VirtualBox VM with Kubernetes master setup
* Doesn't install kubectl

## MicroK8s
* Installs Kubernetes (without Docker Engine) on localhost (Linux)
* Uses snap (rather then apt or yum) for install
* Control the MicroK8s service via microk8s. commands
* kubectl accessable via microk8s.kubectl
* Add CoreDNS for services to work
* microk8s.enable dns
* Add an alias to your shell (.bash\_profile)
* alias kubectl=microk8s.kubectl 

## Kubernetes Container Abstractions
* Pod: one or more containers running together on one Node
* Basic unit of deployment. Containers are always in pods
* Controller: For creating/updating pods and other objects
* Many types of Controllers inc. Deployment, ReplicaSet,

## StatefulSet, DaemonSet, Job, CronJob, etc.
* Serv ice: network endpoint to connect to a pod
* Namespace: Filtered group of objects in cluster
* Secrets, ConfigMaps, and more