## Kubernetes Run, Create, and Apply
* Kuberentes is evolving, and so is the CLI
* We get three ways to create pods from the kubectl CLI

\> **kubectl run** (changing to be only for pod creation)

\> **kubectl create** (create some resources via CLI or YAML)

\> **kubectl apply** (create/update anything via YAML)
* For now we'll just use run or create CLI
* Later we'll learn YAML and pros/cons of each

## Creating Pods with kubectl
* Are we working?

\> **kubectl version**
* Tw o ways to deploy Pods (containers): Via commands, or via YAML
* Let's run a pod of the nginx web server!

\> **kubectl create deployment my-nginx --image nginx**
* Let's list the pod

\> **kubectl get pods**
* Let's see all objects

\> **kubectl get all**

## Pods -> ReplicaSet -> Deployment

Cleanup
* Let's remove the Deployment

\> **kubectl delete deployment my-nginx**

## Scaling ReplicaSets
* Start a new deployment for one replica/pod

\> **kubectl create deployment my-apache --image httpd**
* Let's scale it up with another pod

\> **kubectl scale deploy/my-apache --replicas 2**

\> **kubectl scale deployment my-apache --replicas 2**
* those are the same command
* deploy = deployment = deployments

## What Just Happened? kubectl scale

Don't Cleanup
* We'll use these httpd containers in the next lecture

## Inspecting Deployment Objects

\> **kubectl get pods**
* Get container logs

\> **kubectl logs deployment/my-apache --follow --tail 1**
* Get a bunch of details about an object, including events!

\> **kubectl describe pod/my-apache-xxxx-yyyy**
* Watch a command (without needing watch)

\> **kubectl get pods -w**
* In a separate tab/window

\> **kubectl delete pod/my-apache-xxxx-yyyy**
* Watch the pod get re-created

Cleanup
* Let's remove the Deployment

\> **kubectl delete deployment my-apache**

