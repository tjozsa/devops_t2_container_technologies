## Replicated and global service deployments
* For a replicated service, you specify the number of identical tasks you want to run.
* A global service is a service that runs one task on every node.
* There is no pre-specified number of tasks.
* Each time you add a node to the swarm, the orchestrator creates a task and the scheduler assigns the task to the new node.
![](img/replicated.png)
## Swarm network mesh - Overlay Multi-Host Networking
* Just choose --driver overlay when creating network
* Basically, it is a swarm wide bridge network
* For container-to-container traffic inside a single Swarm
* Optional IPSec (AES) encryption on network creation
* Each service can be connected to multiple networks

![](img/multinode.png)

## Routing mesh
* Routes ingress (incoming) packets for a Service to proper Task
* Spans all nodes in Swarm
* Uses IPVS from Linux Kernel
* Load balances Swarm Services across their Tasks
* Two ways this works:
* Container-to-container in an Overlay network (uses Virtual IPs)
* External traffic incoming to published ports (all nodes listen)